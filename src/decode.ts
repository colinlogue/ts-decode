import { Result, ok, err } from "@colinlogue/ts-result";

// =====================
// ==  Decoder types  ==
// =====================

export type Decodable = number | string | boolean | null | {[key: string]: Decodable} | Decodable[];

export type DecoderFunc<T> = (val: Decodable) => Result<T>;

export type DecoderLike<T> = Decoder<T> | DecoderFunc<T>;

export type OptionalField<T> = {optional: boolean, decode: DecoderLike<T>};

export type DecodeScheme<T extends {[key: string]: Decodable}> = {[Prop in keyof T]: undefined extends T[Prop] ? (OptionalField<Exclude<T[Prop], undefined>> | DecoderLike<Exclude<T[Prop], undefined>>) : DecoderLike<T[Prop]>};

// ===============
// ==  Helpers  ==
// ===============

export function compare(val1: Decodable, val2: Decodable): boolean {
  if (val1 && typeof val1 === 'object' && val2 && typeof val2 === 'object') {
    if (Array.isArray(val1)) {
      if ( ! Array.isArray(val2)) return false;
      if (val1.length !== val2.length) return false;
      for (let i = 0; i < val1.length; ++i) {
        if ( ! compare(val1[i]!, val2[i]!)) return false;
      }
      return true;
    }
    else if (Array.isArray(val2)) {
      return false;
    }
    const keys1 = Object.keys(val1);
    const keys2 = Object.keys(val2);
    if (keys1.length !== keys2.length) return false;
    for (const k in keys1) {
        if ( ! keys2.includes(k)) return false;
        if ( ! compare(val1[k]!, val2[k]!)) return false;
    }
    return true;
  }
  return val1 === val2;
}

export function isDecodable(val: unknown): val is Decodable {
  return typeof val === 'number'
    || typeof val === 'string'
    || typeof val === 'boolean'
    || val === null
    || (Array.isArray(val) && val.every(isDecodable))
    || (typeof val === 'object' && Object.values(val).every(isDecodable));
}

function repr(val: unknown): string {
  return JSON.stringify(val);
}

export function applyDecoderLike<T>(f: DecoderLike<T>, val: Decodable): Result<T> {
  if (f instanceof Decoder) {
    return f.decode(val);
  }
  return f(val);
}

// =====================
// ==  Decoder class  ==
// =====================

export class Decoder<T> {

  constructor(public readonly decode: DecoderFunc<T>) {}

  static succeed<T>(val: T): Decoder<T> {
    return new Decoder(_ => ok(val));
  }

  static fail(msg: string): Decoder<any> {
    return new Decoder(_ => err(msg));
  }

  map<T2>(f: (val: T) => T2): Decoder<T2> {
    return new Decoder(val => this.decode(val).map(f));
  }

  then<T2>(f: (val: T) => DecoderLike<T2>): Decoder<T2> {
    return new Decoder(val => {
      const res = this.decode(val);
      if (res.ok) {
        const decoder2 = f(res.value);
        return applyDecoderLike(decoder2, val);
      }
      return err(res.error);
    });
  }

  check(pred: (val: T) => boolean, msg: string = ''): Decoder<T> {
    if (msg !== '') msg = ': ' + msg;
    const mkErr = (val: T) => err(`${repr(val)} does not satisfy predicate` + msg);
    return new Decoder(
      val =>
        this.decode(val).then(x => pred(x) ? ok(x) : mkErr(x)));
  }

}

// ==========================
// ==  Primitive decoders  ==
// ==========================

export const decodeNumber: DecoderFunc<number> =
  val =>
    typeof val === 'number'
      ? ok(val)
      : err(`${repr(val)} is not a number`);

export const number: Decoder<number> = new Decoder(decodeNumber);

export const decodeBoolean: DecoderFunc<boolean> =
  val =>
    typeof val === 'boolean'
      ? ok(val)
      : err(`${repr(val)} is not a boolean`);

export const boolean: Decoder<boolean> = new Decoder(decodeBoolean);

export const decodeString: DecoderFunc<string> =
  val =>
    typeof val === 'string'
      ? ok(val)
      : err(`${repr(val)} is not a string`);

export const string: Decoder<string> = new Decoder(decodeString);

export const decodeNull: DecoderFunc<null> =
  val =>
    val === null
      ? ok(null)
      : err(`${repr(val)} is not null`);

export const nothing: Decoder<null> = new Decoder(decodeNull);

export function decodeExact<T extends Decodable>(expected: T): DecoderFunc<T> {
  return val =>
    compare(val, expected)
      ? ok(expected)
      : err(`${repr(val)} is not ${expected}`);
}

export const exact = <T extends Decodable>(expected: T): Decoder<T> =>
  new Decoder(decodeExact(expected));

export const decodeAny: DecoderFunc<Decodable> = ok;

export const any: Decoder<Decodable> = new Decoder(decodeAny);

// =========================
// ==  Compound decoders  ==
// =========================

export function decodeArray<T>(decodeVal: DecoderLike<T>): DecoderFunc<T[]> {
  return val => {
    if (Array.isArray(val)) {
      const acc = [];
      for (let i = 0; i < val.length; ++i) {
        const v = val[i];
        if (v === undefined) {
          return err(`array index ${i} is empty`);
        }
        const res = applyDecoderLike(decodeVal, v);
        if (res.ok) {
          acc.push(res.value);
        }
        else {
          return err(`while decoding array index ${i}: ${res.error}`);
        }
      }
      return ok(acc);
    }
    return err(`${repr(val)} is not an array`);
  }
}

export const array = <T>(decodeVal: DecoderLike<T>): Decoder<T[]> =>
  new Decoder(decodeArray(decodeVal));

export const decodeEntries = <T>(decodeVal: DecoderLike<T>): DecoderFunc<[string, T][]> => {
  const f = (acc: [string, T][], k: string, v: Decodable): Result<[string, T][]> => {
    return applyDecoderLike(decodeVal, v).map(x => { acc.push([k, x]); return acc; });
  };
  return val =>
    typeof val === 'object' && !Array.isArray(val) && val !== null
      ? Object.entries(val).reduce<Result<[string, T][]>>((res, [k, v]) => res.then(acc => f(acc, k, v)), ok([]))
      : err(`${repr(val)} does not have entries`);
}

export const entries = <T>(decodeVal: DecoderLike<T>): Decoder<[string, T][]> =>
  new Decoder(decodeEntries(decodeVal));

/**
 * Helper function for specifying optional fields in a DecoderScheme.
 */
 export function optional<T>(decode: Decoder<T>): OptionalField<T> {
  return {
    optional: true,
    decode
  };
}

function _unwrapDecoder<T>(def: DecoderLike<T> | OptionalField<T>): OptionalField<T> {
  if ('optional' in def) {
    return def;
  }
  return {
    optional: false,
    decode: def
  };
}

export function decodeScheme<T extends {[key: string]: Decodable}>(def: DecodeScheme<T>): DecoderFunc<T> {
  return val => {
    if (typeof val === 'object' && val !== null && !Array.isArray(val)) {
      const obj: {[key: string]: Decodable} = {};
      for (const name of Object.keys(def)) {
        const {optional, decode} = _unwrapDecoder(def[name]!);
        const fieldVal = val[name];
        if (fieldVal === undefined) {
          if (optional) {
            continue;
          }
          else {
            return err(`field ${name} missing from ${repr(val)}`);
          }
        }
        const res = applyDecoderLike(decode, fieldVal);
        if (res.ok) {
          if (res.value !== undefined) {
            obj[name] = res.value;
          }
        }
        else if ( ! optional) {
          return err(`while decoding field ${name}: ${res.error}`);
        }
      }
      // I'm a little suspicious of this cast, but I'm not sure of a way
      // to get TypeScript to confirm that all the keys were assigned.
      // They should have been, as long as Object.keys is guaranteed to
      // go over all the keys from keyof.
      return ok(obj as T);
    }
    return err(`${repr(val)} is not a decodable object`);
  }
}

export const scheme = <T extends {[key: string]: Decodable}>(def: DecodeScheme<T>): Decoder<T> =>
  new Decoder(decodeScheme(def));

/**
 * Note that it may be necessary to explicitly state the type in order
 * to make the type checker happy, if tje decoders don't all have the
 * same type.
 * 
 * @param decoders The list of decoders to try, in order. The returned
 * value is the first that succeeds.
 */
export function decodeOneOf<T>(...decoders: DecoderLike<T>[]): DecoderFunc<T> {
  return val => {
    const failures = [];
    for (const decode of decoders) {
      const res = applyDecoderLike(decode, val);
      if (res.ok) return res;
      failures.push(res.error);
    }
    return err(`all decoders failed: ${failures}`);
  };
}

export const oneOf = <T>(...decoders: DecoderLike<T>[]): Decoder<T> =>
  new Decoder(decodeOneOf(...decoders));

export function satisfy<T>(decode: DecoderLike<T>, pred: (val: T) => boolean, msg?: string | ((val: T) => string)): DecoderFunc<T> {
  const errMsg: (val: T) => string =
    typeof msg === 'string'
      ? x => `${repr(x)}: ${msg}`
      : typeof msg === 'function'
      ? msg
      : x => `${x} does not satisfy predicate`;
  return val => applyDecoderLike(decode, val).then(x => pred(x) ? ok(x) : err(errMsg(x)));
}


// ========================
// ==  Derived decoders  ==
// ========================

export const decodeInteger: DecoderFunc<number> = satisfy(decodeNumber, Number.isInteger, x => `${x} is not an integer`);

export const integer: Decoder<number> = new Decoder(decodeInteger);
