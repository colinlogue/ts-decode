import { Test, TestCase, TestSuite } from "testyts/build/testyCore";
import { Result } from "@colinlogue/ts-result";
import { array, boolean, Decodable, Decoder, number, oneOf, optional, satisfy, scheme, string } from "../src/decode";
import { expect } from "chai";



type UserInfo = {
  username: string,
  password: string
}

function validateUsername(username: string): boolean {
  return /^[a-zA-Z0-9_]+$/.test(username);
}

function validatePassword(password: string): boolean {
  return /[a-z]/.test(password) && /[A-Z]/.test(password) && /[0-9]/.test(password);
}

function unwrap<T>(res: Result<T>): T | string {
  return res.ok ? res.value : res.error;
}


@TestSuite('decode tests')
export class DecodeTests {

  @Test('user info test')
  userInfoTest() {

    const decoder: Decoder<UserInfo> = scheme({
      username: string,
      password: string
    });

    const testVal: UserInfo = {
      username: "Ronald McDonald",
      password: "cheeseburger"
    };

    const res = decoder.decode(testVal);

    expect(unwrap(res)).to.deep.equal(testVal);

  }

  @Test('validated user info test')
  @TestCase('invalid both', 'Ronald McDonald', 'cheeseburger', false)
  @TestCase('invalid password', 'ronnyMcD', '1234', false)
  @TestCase('invalid username', '###', 'aZ9', false)
  @TestCase('valid', 'ronnyMcD', 'ch33Z3burg3r', true)
  validatedUserInfoTest(username: string, password: string, isValid: boolean) {

    const decoder: Decoder<UserInfo> = scheme({
      username: satisfy(string, validateUsername),
      password: satisfy(string, validatePassword)
    });

    const testVal: UserInfo = {username, password};

    const res = decoder.decode(testVal);

    if (isValid) {
      expect(unwrap(res)).to.deep.equal(testVal);
    }
    else {
      expect(typeof unwrap(res)).to.equal('string');
    }

  }

  @Test('nested scheme')
  nestedScheme() {

    const decoder = scheme({
      a: scheme({
        b: boolean,
        c: number
      }),
      d: string
    });

    const testVal = {
      a: {
        b: true,
        c: 42
      },
      d: "d"
    };

    const res = decoder.decode(testVal);
    expect(unwrap(res)).to.deep.equal(testVal);

  }

  @Test('array of string or number')
  @TestCase('all numbers', [1,2,3], true)
  @TestCase('all string', ['a','b','c'], true)
  @TestCase('empty', [], true)
  @TestCase('invalid', [1,2,false,4], false)
  arrays(testVal: Decodable[], isValid: boolean) {

    const decoder = array(oneOf<string|number>(string, number));

    const res = decoder.decode(testVal);

    if (isValid) {
      expect(unwrap(res)).to.deep.equal(testVal);
    }
    else {
      expect(res.ok).to.be.false;
    }

  }

  @Test('optional fields')
  optionalFields() {

    const decoder = scheme<{x?: number, y: number}>({
      x: optional(number),
      y: number
    });

    const testData1 = {
      x: 1,
      y: 2
    };

    const res1 = decoder.decode(testData1);
    expect(unwrap(res1)).to.deep.equal(testData1);

    const testData2 = {
      y: 3
    };

    const res2 = decoder.decode(testData2);
    expect(unwrap(res2)).to.deep.equal(testData2);
    
  }

  @Test('optional field required')
  optionalFieldRequired() {

    const decoder = scheme<{x?: number, y: number}>({
      x: number,
      y: number
    });

    const testData1 = {
      x: 1,
      y: 2
    };

    const res1 = decoder.decode(testData1);
    expect(unwrap(res1)).to.deep.equal(testData1);

    const testData2 = {
      y: 3
    };

    const res2 = decoder.decode(testData2);
    expect(res2.ok).to.be.false;

  }

}